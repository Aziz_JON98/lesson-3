// Lesson 3
// function fizzBuzz (x) {
//     // your code here
   
//         if (x%3==0){
//             if(x%5==0){
//                 console.log("FizzBuzz")
//             } else {
//                 console.log("Fizz")
//             }
//         } else if(x%5==0){
//             console.log("Buzz")
//         } else{
//             console.log(x)
//         }
//     }
// for (let i=1; i<=100; i++){
//     fizzBuzz(i);
// }

function filterArray (list) {
    // your code here
    let arr = [];
    for(i of list){
        if(Array.isArray(i)){
            arr.push(...i)
            // console.log(arr)
        }
    }
    return Array.from(new Set(arr)).sort((a,b)=>{return a-b;});
}

const letList = [
    [2], 
    23,
    'dance',
    [7,8,9, 7],
    true, 
    [3, 5, 3],
    [777,855, 9677, 457],
]

console.log(filterArray(letList))